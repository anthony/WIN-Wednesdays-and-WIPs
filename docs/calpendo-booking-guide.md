---
layout: default
title: Calpendo booking guide
has_children: true
nav_order: 3
---

# Calpendo booking guide
{: .fs-9 }

Find out how to use Calpendo to book your WIP or WIN Wednesday Seminar
{: .fs-6 .fw-300 }

---

**Contents**
- [What is Calpendo?](#what-is-calpendo)
- [How do I get a Calpendo account?](#how-do-i-get-a-calpendo-account)

## What is Calpendo?

Calpendo is a customisable "facilities management system". We use Calpendo at WIN to book scanning time, research labs, testing rooms, meeting rooms, research equipment, and even bicycle parking spaces! Calpendo is well suited for these tasks as we can closely define rules for the booking and control access. Some resources are available for everyone, others can only be booked by people who have completed the required training.

We use Calpendo to book WIN Wednesday presentations as it minimises the friction and email traffic in making your booking - you can instantly see what slots are available and edit your booking directly. Calpendo can also collect the required documentation (for example signed recording releases) and send automated reminders to help you prepare for your presentation.

## How do I get a Calpendo account?

Calpendo is available to all WIN members, but you need an account to access it.

To create your Calpendo account, go to [http://calpendo.fmrib.ox.ac.uk/](http://calpendo.fmrib.ox.ac.uk/) and "sign in using Shibboleth". Enter your Shibboleth ("SSO") username and password.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](../../img/calpendo-shibboleth.png)

If your Shibboleth account has not already been granted access to calpendo, you will be invited to complete an account request form. This form will be sent to the administrator of your department to verify you as a user. It is important to correctly identify your Department so the form is directed appropriately.

**Creating accounts requires the manual intervention of WIN Administrators. This means it is not instantaneous. Please allow 1 week for your account to be issued.** Please contact sebastian.rieger@psych.ox.ac.uk to follow up if your account has not been created in \> 1 week following your request.

Accounts may be suspended if there is no Calpendo activity for \> 1 year. If your account has been suspended, please contact sebastian.rieger@psych.ox.ac.uk to request that it is reinstated.
