---
layout: default
title: WIN Wednesday Coordinators
parent: Calpendo booking guide
has_children: false
nav_order: 2
---

# Guide for WIN Wednesday Coordinators
{: .fs-9 }

How to use Calpendo to book your WIN Wednesday Seminar
{: .fs-6 .fw-300 }

---

**Contents**
- [What are WIN Wednesday Coordinators?](#what-are-win-wednesday-coordinators)
- [How to book WIN Wednesday seminars](#how-to-book-win-wednesday-seminars)
- [How to amend or cancel your WIN Wednesday seminar](#how-to-amend-or-cancel-your-win-wednesday-seminar)
- [Approval of your booking](#approval-of-your-booking)
- [What if you need to change the date/time of your seminar?](#what-if-you-need-to-change-the-date-time-of-your-seminar)
- [Additional resources: Introductory slides](#additional-resources-introductory-slides)
- [Additional resources: Call Recording in MS Teams](#additional-resources-call-recording-in-ms-teams)



## What are WIN Wednesday Coordinators?

WIN Wednesday Coordinators are the WIN members responsible for convening presentations with content themed around methods, career development, equality, diversity and inclusion, publication round ups, and guest speakers. They are happy to receive your suggestions for content. Find out [more about the WIN Wednesday Coordinators on the WIN Intranet](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/Book%20WIN%20Wednesday.aspx)

Before the start of each academic year, the WIN Wednesday schedule is decided and slots allocated to each of these topics, along with space for special announcements or activities. WIP presentations can be scheduled for the same WIN Wednesday session if time allows. The allocation of WIN Wednesday content will be communicated with the WIN Wednesday Coordinators before the start of the academic year. [Allocations for 2021-2022 are available to the WIN Wednesday Coordinators on Microsoft Teams](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-- 2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2).

## How to book WIN Wednesday seminars

WIN Wednesday Coordinators are required to book their allocated slots in Calpendo using the process described below. Using Calpendo allows for simplified collation of required documentation, automated email reminders and flexibility of scheduling.

### 1. Create an account

Ensure you have an active Calpendo account. This may take up to one week to issue or reinstate. You can check if your account is active by attempting to [log in using Shibboleth](#2-log-into-calpendo-using-shibboleth) as described below. If you are unable to log in, see [How do I get a Calpendo account](../calpendo-booking-guide#how-do-I-get-a-calpendo-account).

### 2. Log into Calpendo using Shibboleth

Go to [http://calpendo.fmrib.ox.ac.uk/](http://calpendo.fmrib.ox.ac.uk/).

Click the "Shibboleth" button indicated below.

On the following page use your University of Oxford Shibboleth SSO to sign into the Calpendo homepage.

![calpendo sign in page with arrow highlighting Shibboleth sign in button](../../img/calpendo-shibboleth.png)

### 3. Select the WIN Wednesday resource

Select the WIN Wednesday resource from the list of available calendars.

Click the "Calendars" tab on the top left of the Calpendo page, then select WIN Wednesdays as shown below.

![calpendo booking calendar with arrows highlighting the calendar drop down and WIN Wednesday resource](../../img/calpendo-resource.png)

### 4. Find your allocated slot

Use the week navigation buttons near the top of the page to find to the slot you have been allocated. [Allocations for 2021-2022 are available to WIN Wednesday Coordinators on Microsoft Teams](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2).

![calpendo booking calendar for WIN Wednesdays with arrows highlighting the week navigation buttons and an available slot](../../img/calpendo-available.png)

### 5. Click to open the "new booking" window

Click your allocated slot in the calendar to open the "new booking" window. Enter the required details as described below.

![calpendo new booking window for a WIN Wednesday seminar with arrows highlighting: 1) end time; 2) email reminder notice period; 3) description text box; 4) Title, Authors and Abstract text box; 5) Recording consent upload button](../../img/calpendo-newbooking-coord.png)

#### 1. Start and end times

The start time of your booking will depend on whether time is reserved for a WIP presentation. Please refer to the [allocation for 2021-2022](https://teams.microsoft.com/l/file/11779894-604e-4db1-9b3e-e6bde563aaaa?tenantId=cc95de1b-97f5-4f93-b4ba-fe68b852cf91&fileType=xlsx&objectUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk%2FDocuments%2FMicrosoft%20Teams%20Chat%20Files%2FWIN%20Wednesday%20Series%202021-2022.xlsx&baseUrl=https%3A%2F%2Funioxfordnexus-my.sharepoint.com%2Fpersonal%2Fndcn0919_ox_ac_uk&serviceName=p2p&threadId=19:ecaccdfe326c4351a9bf7ff75f1ca8c4@thread.v2) to see if there is space for a WIP in the same session as your seminar (note there is space for a WIP unless the row explicitly says "No WIP").

*If there is a WIP in the same session as your seminar, your seminar will last 40 minutes. Please set the start time to 12:20 and the end to 13:00.*

*If there is no WIP in the same session as your seminar, your seminar will last 60 minutes. Please set the start time to 12:00 and the end to 13:00.*

*Please ensure that all speakers are informed of the appropriate duration of their presentation.*

**Please note that if a WIP presentation is booked in that session, it will be held before your seminar. Please ensure, however, that all speakers for your seminar are present for the duration 12:00-13:00 the session.**

**We request that all presenters join the call 10 minutes before the start of the meeting to test audio-visual equipment with the host.**

#### 2. Reminder notice period

This sets the date which Calpendo will send you a reminder about your upcoming booking. This reminder will include the details of your booking as you have entered them. We suggest you use a minimum of a **5 day** notice period, so you can review and amend your booking details before they are distributed in the Monday Message. See the process below to [amend a booking](#how-to-amend-or-cancel-your-win-wednesday-seminar).

You can change the default reminder or notice period in your personal Calpendo settings. To do this, click "Settings" (top right) then "booking reminders" from the list on the left.

#### 3. Description

Please enter name of your seminar series, for example "WIN Wednesday EDI Series". This makes it easier for the WIN Wednesday Administrators to review your booking appropriately.

#### 4. Title, Authors and Abstract

Please enter the title of your seminar, the authors (presenters) and an  abstract. This information will be circulated in the Monday Message in the week of your presentation.

We understand that you may not have details of your Seminar yet. This is not a problem at this stage. We ask, however, that you use the Calpendo or other notice functions to remind you to enter the relevant information into the booking as early as possible, as this is the information which will be used to populate the Monday Message.

**Bookings will not be approved until these details are complete.**

#### 5. Recording consent upload button

A signed speaker recording release ("Recording Consent") is required to record your WIN Wednesday Seminar. Recording allows other WIN members to review and learn from your seminar. A signed release is required for everyone who intends to present in the main part of the seminar. A release is not required for people who only intend to be on screen during the Q&A as this will not be recorded.

Download a [speaker recording release](https://help.it.ox.ac.uk/sites/default/files/help/documents/media/downloadable_legal_form_pack_3-10-22.zip) from Central IT. **Please distribute the speaker recording release pack to your intended presenters and ask them to return their signed copy to you.** Presenters should sign using the MS Word .docx version, or .pdf if MS Word is not available. If using the .pdf, please create text boxes to write over the appropriate white spaces.

Once returned, please upload the release(s) to your booking. Click the "Choose File" button to locate the release file(s) and add it to your booking. **Bookings will not be approved until at least one signed release has been uploaded.**

*We appreciate that some speakers may be hesitant to have their presentations recorded. Please reassure them that presentation will only be available internally to members of the University of Oxford (SSO is required). Please [contact the WIN Wednesday Administrators](../calpendo-booking-guide-support) if there are any concerns.*


#### 6. In-person or online speaker participation

To help us plan the meeting, we need to know whether the speaker will be present in-person from in the [Cowey Room in the FMRIB Annex](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-fmrib/meeting-rooms), the [Meeting Room in OHBA](https://www.win.ox.ac.uk/about/how-to-find-us/finding-win-ohba), or joining the Teams call online.

Please identify where the speaker will be joining from by selecting from the drop down list. **Note the default is in-person - please update this if the speaker will be joining online.**

#### 7. Submit you booking

Click the "Create Booking" button to submit your booking for approval. Your booking will be sent to a WIN Wednesday administrator and reviewed for completeness before it is approved.

You will receive an email confirmation that your booking has been received. This email will be sent to the address associated with your Shibboleth account.

You will receive an email notification once your booking has been approved, or you will be asked for further information. **You will be contacted by an administrator if the - booking information is not complete by 17:00 Friday of the week proceeding your slot.**

## How to amend or cancel your WIN Wednesday seminar

**Booking details (such as names, abstract) can be amended by the booker up to 12:00 on the Friday before the scheduled seminar. If you need to amend your booking after that time, please [contact a WIN Wednesday administrator](../calpendo-booking-guide-support).**

To amend your booking, return to the WIN Wednesday resource calendar following steps [2. Log into Calpendo using Shibboleth](#2-log-into-calpendo-using-shibboleth) and [3. Select the WIN Wednesday resource](#3-select-the-win-wednesday-resource) above.

Edit the information as required and click "Update Booking" to save the changes. To cancel your booking, click "Cancel Booking". You will be notified that your booking has been cancelled.

- ![calpendo edit booking window for WIN Wednesdays with arrows highlighting the update and cancel booking buttons](../../img/calpendo-amend-coord.png)

## Approval of your booking

You will get an email from Calpendo when your booking has been approved. Alternatively you may receive an email with an instruction to add or clarify some detail.

Once your booking has been approved you are still able to edit it, but we would prefer if you didn't! Please [contact an administrator](../calpendo-booking-guide-support) if you need to make any changes to your booking.

Your booking will be cancelled if it is not complete by 17:00 on the Friday preceding your booked slot. You will receive and email notification if your booking is cancelled.

## What if you need to change the date/time of your seminar?

In some circumstances it may be necessary to hold your seminar outside of the usual Wednesday 12:00-13:00 slot, for example to meet the availability of external presenters. In these cases, please [contact a WIN Wednesday Administrators](../calpendo-booking-guide-support) and ask them to open a slot in Calpendo at the required time.

## Additional resources: Introductory slides

We have created a set of template slides (splash screens) for you to present at the start of your seminar. These splash screens can be particularly useful for online presentations, to let people know they are in the correct meeting and display the recording notice. The slides also act as a "holding stage" to fill the dead air while viewers join the call.

Templates for each WIN Wednesday theme are available for download from the gitlab repository supporting this site:
- [Early Career Researcher Skills](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-ECRSkills-template.ppt)
- [Equality, diversity and inclusion series](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-EDI-template.ppt)
- [Methods series](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-Methods-template.ppt)
- [Publication round-up](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-Pubs-template.ppt)
- [Seminar / Guest speaker](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-SeminarGuest-template.ppt)
- [WIP](https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/blob/master/img/introSplash-templates/WINWed-introSplash-WIP-template.ppt)

### How to use

The slide deck contains two custom slide shows:
1. "Custom Show loop" - Rotates through slides 1:3 continuously. All slides contain a "home" action button.
2. "Custom Show home" - Shows only slide 4 (the "home" slide).

When you start the presentation from slide one it will begin the "loop" show. When you are ready to start the session, you can either stop the slide show or click the "home" icon (action button) on the bottom right of any of the looping slides. Clicking the home action button starts the "home" custom show, which displays only slide 4.

### What to edit

Slide 2 contains information which should be updated for each seminar:
- Date
- Presenter(s)
- Title

### Images

The illustrations are via [undraw](https://undraw.co/search). These are available on a fully open license, and customisable to a colour scheme. The images included are customised to the WIN logo colour: Hex #750A00. Please retain the image and slide theme credit on slide 3 if you use these templates.

## Additional resources: Call Recording in MS Teams
The [WIP coordinator](../calpendo-booking-guide-support) will be the host of the call for all seminars where there is a WIP slot. For other WIN Wednesday Seminars where there is no WIP slot, the WIN Wednesday Coordinator will host the call. The host will be responsible for recording the call.

Where the WIN Wednesday Coordinators is hosting the call, you need to request that is feature is enabled on your Nexus account. **Please see [this guide from IT](https://help.it.ox.ac.uk/record-a-meeting-in-teams) to request that recording is enabled on your account (minimally 48h before the call is scheduled) and for instructions on how to record the call.**
