Thank you for visiting the GitLab repository for the WIN Wednesday and WIP guides.

**This repository holds all the material necessary to produce the rendered pages available here: https://open.win.ox.ac.uk/pages/cassag/WIN-Wednesdays-and-WIPs/. You will not see the contents of *this* document rendered on the linked website.**

This page is a hub to give you some information about WIP and Calpendo guides. Jump straight to one of the sections below, or scroll down to find out more.

* [What are we doing? (And why?)](#what-are-we-doing-and-why)
* [What is happening now?](#what-is-happening-now)
* [How can you contribute?](#how-can-you-contribute)

## What are we doing? (And why?)

The Work in Progress (WIP) meetings are scheduled as part of the WIN Wednesday meetings that take place on Wednesdays 12:00 - 13:00. Everyone who is affiliated with the WIN is welcome to attend these meetings.

WIPs provide an informal and constructive forum for researchers to get feedback on planned research projects from a diverse WIN audience. In some cases, WIP presentations are a mandatory stage in gaining the appropriate approvals to conduct new data collection using WIN facilities.

We are experimenting with some options for bringing a more diverse set of WIN members to present their work as a WIP. At the same time, we are overhauling the way WIPs (and wIN Wednesdays) are booked, to streamline the administration.

### Problem 1: Range of WIPs

WIN research can be broadly divided into four core themes:

- Analysis Research
- Basic Neuroscience
- Clinical Neuroscience
- Physics Research

An analysis of WIP presenters in 2019 (taken as an indicative pre-pandemic year), shows that most presentations are lead by researchers associated with Basic or Clinical Neuroscience, with fewer from Physics group or Analysis groups.

The impact of this uneven research theme distribution is multifactoral:

1. WIN members do not have an accessible opportunity to learn about the full breadth of research being conducted at WIN.
2. Physics and Analysis researchers do not have an open forum for feedback from applied researchers regarding the translational and clinical aspects of their work.
3. Physics and Analysis students do not have an equal opportunity to present their research to a broad audience.

The proposed changes to the practice and scope of WIP meetings seeks to address this uneven distribution of WIP presentations to: 1) more completely communicate the breadth of WIN research; 2) highlight and encourage researchers to consider the translational value of their work across the centre; 3) give equal opportunity to WIN students and Early Career Researchers to present to a broad and receptive audience.

#### Solution 1: Extended content

WIN recognises that considerable research effort and expertise goes into projects which do not involve the collection of new experimental or observational data. We would like to promote knowledge exchange around such projects by broadening the range of activities which are deemed appropriate for WIP presentations. This might include projects involving:
- secondary data analysis;
- new software applications or extensions;
- training materials;
- experimental protocols;
- public engagement activities;

#### Solution 2: Local theme promotors

The Core WIN individuals named below have agreed to promote WIP participation in their respective themes, in alignment with the ethos of informal and constructive interdisciplinary feedback with a specific focus on promoting conversations around translation.  

- Analysis: taylor.hanayik@ndcn.ox.ac.uk
- Physics: mohamed.tachrount@ndcn.ox.ac.uk or aaron.hess@ndcn.ox.ac.uk
- Preclinical: claire.bratley@ndcn.ox.ac.uk
- Clinical OHBA: clare.odonoghue@psych.ox.ac.uk
- Clinical FMRIB: jessica.walsh@ndcn.ox.ac.uk
- Clinical Neurosciences: marieke.martens@psych.ox.ac.uk
- Cognitive Neuroscience (MRI): sebastian.rieger@psych.ox.ac.uk
- Cognitive Neuroscience (MEG/EEG): anna.camera@psych.ox.ac.uk
- All other themes or questions: cassandra.gouldvanpraag@psych.ox.ac.uk

These promotors will work to 'nudge' people in their teams to consider giving a WIP. They will keep their ear to the ground regarding new projects and be a first point of contact for questions.

### Problem 2: Booking administration and process knowledge

Previous to this resource, when a researcher wished to present a WIP, they relied on word-of-mouth and a few scattered guides to learn how to book the meeting and what a WIP entailed. Such undocumented processes contribute to poor inclusivity through the potential for unequal access to knowledge.

The old process also entailed several email exchanges between researchers, the WIP lead, and WIN Administrators, to identify available slots and receive appropriate documentation and notices. This is an inefficient use of resources, and delays in communication can become problematic when there is high demand for presentation slots.

The meeting slots available for WIP presentations are also shared with other WIN Wednesday content (such as training and [Equality, Diversity and Inclusivity](https://www.win.ox.ac.uk/about/edi) presentations). The Coordinators of WIN Wednesday content would also benefit from an efficient solution for booking their meetings against a shared calendar and collating appropriate documentation.  

#### Solution 1: Calpendo for WIP and WIN Wednesday booking

[Calpendo](https://calpendo.fmrib.ox.ac.uk/bookings/) is used throughout WIN to book research facilities and meeting spaces. We propose that researchers use Calpendo to directly book their WIP slot, to improve efficiency and reduce the administrative burden in arranging presenters.

Calpendo can also be used to collate the required documentation and set deadlines for required documentation. For example, it may be beneficial to make it mandatory to upload some information (for example project title and abstract) at the point of booking. The file [booking-proces.md](booking-process.md) is being used to track the current logistics and communications around booking, and will be used to create a specification for the Calpendo facility.

If required, Calpendo may also make it easier to produce reports on historical WIP activity, to review the efficacy of any initiatives to improve uptake.

WIN Wednesday coordinators have also agreed to use Calpendo for booking other types of speakers and events.

### Solution: Intranet WIP guide

[A complete guide on the process and value of WIPs has been published](https://open.win.ox.ac.uk/pages/cassag/WIN-Wednesdays-and-WIPs/docs/WIP-guide/) and is linked from the [WIN intranet](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/SitePages/Book%20WIN%20Wednesday.aspx). This guide will be advertised in Monday messages, so researchers or group leaders new to the process will have ready access to all necessary information.


## What are we doing?

The proposal for making these changes was presented to the WIN management board on 14th July. We are now testing the resources and disseminating the guide for feedback.

## How can you contribute?

We are grateful for your feedback on your experience of using either the [WIP](https://open.win.ox.ac.uk/pages/cassag/WIN-Wednesdays-and-WIPs/docs/WIP-guide/) or [Calpendo](https://open.win.ox.ac.uk/pages/cassag/WIN-Wednesdays-and-WIPs/docs/calpendo-booking-guide/) guides published from this repository.

To provide your feedback, please either:
1. Leave a comment on the [WIP]https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/issues/2 or [Calpendo]https://git.fmrib.ox.ac.uk/cassag/WIN-Wednesdays-and-WIPs/-/issues/1 issues.
2. Start a new issue if you have more general feedback
3. Contact the WIP administrator by email: cassandra.gouldvanpraag@psych.ox.ac.uk
